class Circlelist {
    constructor(opts) {

        this.opts = opts;
        if (!opts || !opts.el || !opts.radius || !opts.items) {
            throw new Error("Error of creating Circlelist: required option(s) is missed!");
        }
        this.$el = $(opts.el);

        this.$spot = $(`<div class='Circlelist_spot'></div>`);

        window.requestAnimationFrame(() => {
            this.$el
                .addClass('Circlelist')
                .append(`<div class='Circlelist_border'></div>`);

            {
                let diametr = Math.ceil(opts.radius * 2);
                this.$el.css({
                    width: `${diametr}vh`,
                    height: `${diametr}vh`,
                });
            }

            let $inner = $("<div class='Circlelist_inner'></div>");
            for (let i = 0, len = opts.items.length; i < len; i++) {
                let point = this.getPointCoords(i, len);
                let xPosClass =
                    Math.round(point.x) < 0 ? "Circlelist_textbox-left" :
                    Math.round(point.x) == 0 ? "Circlelist_textbox-center" : "Circlelist_textbox-right";
                let yPosClass =
                    Math.round(point.y) < 0 ? "Circlelist_textbox-top" :
                    Math.round(point.y) == 0 ? "Circlelist_textbox-middle" : "Circlelist_textbox-bottom";
                let item = opts.items[i];

                let $point = $("<div>");
                $point
                    .addClass(`Circlelist_point`)
                    .html(`\
<div class="Circlelist_textbox ${xPosClass} ${yPosClass}">\
    <div class="Circlelist_title">${item.title}</div>\
    <div class="Circlelist_desc">${item.subtitle}</div>\
</div>`).css({
                        top: point.y + "vh",
                        left: point.x + "vh"
                    });


                // Attached backgrounds
                let $attachedPic = $(`<div class="Circlelist_pic" style="background-image:url(${opts.items[i].pic})"></div>`);
                $point.hover(() => {
                    $attachedPic.addClass('Circlelist_pic-active');
                }, () => {
                    $attachedPic.removeClass('Circlelist_pic-active');
                });
                this.$spot.append($attachedPic);

                // Attached modals
                {
                    let $modal = $(`
<div style="display:none;"><div class="Mandescr">\
<div class="Mandescr_pic" style="background-image: url(${opts.items[i].pic})"></div>\
<div class="Mandescr_title">${opts.items[i].title}</div>\
<div class="Mandescr_subtitle">${opts.items[i].subtitle}</div>\
<div class="Mandescr_desc">${opts.items[i].description}</div>\
</div></div>`);
                    $modal.appendTo(document.body);
                    $point.find('.Circlelist_textbox:first').on('click', (event) => {
                        event.preventDefault();
                        $.fancybox.open($modal, {
                            toolbar: false,
                            infobar: false,
                            arrows : false,
                            buttons: [
                                'close'
                            ],
                            touch: false,
                            baseTpl :
`<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div>\
<div class="fancybox-inner"><div class="fancybox-stage"></div></div></div>`,
                        });
                    });
                }


                // Insert all items to block
                $inner.append($point);
            }
            this.$el.append([this.$spot, $inner]);
        });

    }

    getPointCoords(index, len) {
        let angle = (((360 / len) * index) - 90) * (Math.PI / 180);
        let radius = this.opts.radius;
        return {
            x: Math.cos(angle) * radius,
            y: Math.sin(angle) * radius,
        };
    }
}